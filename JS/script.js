// Main elements
const quizSection = document.querySelector(".quiz-section");
const questionDiv = document.querySelector(".question");
const category = document.querySelector(".categ");
const options = document.querySelector(".quiz-options");
const result = document.querySelector("#result");
const deck = document.querySelector(".deck");
const scorePlayerElement = document.querySelector(".score-1");
const scoreCpuElement = document.querySelector(".score-2");
const gameSection = document.querySelector(".game-section");
const messageSection = document.querySelector(".display-message");
const messageDesc = document.querySelector(".message-desc");

// Shootout
const shootoutSection = document.querySelector(".shootout-section");
const shootoutQuestionElement = document.querySelector(".shootout-question");
const shootoutInput = document.querySelector(".shootout-answer");
const btnShootout = document.querySelector(".send-shootout");
const shootoutReveal = document.querySelector(".shootout-reveal");
const shootoutPlayerResult = document.querySelector(".result-player");
const shootoutCpuResult = document.querySelector(".result-cpu");
const shootoutCorrectResult = document.querySelector(".result-correct");
let cards = document.querySelectorAll(".card");

// Global variables
let level = "easy";
let winner = "";
let locked = true,
  isActiveGame = true;
let triviaMap = ["", "", "", "", "", "", "", "", ""];
let currentQuestion = 1;
let correctAnswer = "",
  incorrectAnswers = "",
  optionsList = "",
  cpuChoiceElement = "";
let playerScore = 0,
  cpuScore = 0;
let answerElements;

// const selectUrl = (level) =>{
//     if(level === "easy")
//         return `https://opentdb.com/api.php?amount=1&difficulty=easy&type=multiple`;
//     else if(level === "medium")
//         return `https://opentdb.com/api.php?amount=1&difficulty=medium&type=multiple`;
//     else if(level === "hard")
//         return `https://opentdb.com/api.php?amount=1&difficulty=hard&type=multiple`

// }

// Get a random question - EASY difficulty
async function loadEasyQuestion() {
  const APIUrl = `https://opentdb.com/api.php?amount=1&difficulty=easy&type=multiple`;
  const result = await fetch(`${APIUrl}`);
  const data = await result.json();
  console.log(data.results[0]);
  showQuestion(data.results[0]);
}

// Get a random question - MEDIUM difficulty
async function loadMediumQuestion() {
  const APIUrl = `https://opentdb.com/api.php?amount=1&difficulty=medium&type=multiple`;
  const result = await fetch(`${APIUrl}`);
  const data = await result.json();
  console.log(data.results[0]);
  showQuestion(data.results[0]);
}

// Get a random question - HARD difficulty
async function loadHardQuestion() {
  const APIUrl = `https://opentdb.com/api.php?amount=1&difficulty=hard&type=multiple`;
  const result = await fetch(`${APIUrl}`);
  const data = await result.json();
  console.log(data.results[0]);
  showQuestion(data.results[0]);
}

// When there only 1 territory left, get a shootout question
async function loadShootoutQuestion() {
  const result = await fetch("./JS/questions.json");
  const data = await result.json();
  console.log(data);
  showShootoutQuestion(data[Math.floor(Math.random() * data.length)]); // randomize the question we get
}
// Don't allow the user to click on the map
const lockMap = () => {
  locked = true;
  deck.style.pointerEvents = "none";
};

// Allow the user to click on the map
const unlockMap = () => {
  locked = false;
  deck.style.pointerEvents = "all";
};

// Convert html elements to normal text
function HTMLDecode(textString) {
  let doc = new DOMParser().parseFromString(textString, "text/html");
  return doc.documentElement.textContent;
}

// Hide result section and reset it's value
const hideResult = () => {
  result.style.visibility = "hide";
  result.textContent = "";
};

// Generate question's body
const displayQuestion = (data) => {
  correctAnswer = HTMLDecode(data.correct_answer); // convert special symbols
  incorrectAnswers = data.incorrect_answers;
  optionsList = incorrectAnswers;

  // Add the correct and incorrect answers as options
  optionsList.splice(
    Math.floor(Math.random() * (incorrectAnswers.length + 1)),
    0,
    correctAnswer
  );
  questionDiv.innerHTML = `${data.question}`;
  category.innerHTML = `<span class="subject"> Subject: <span>${data.category}</span></span>`;
  // For each answer option, generate an element
  options.innerHTML = `
  ${optionsList
    .map(
      (option, index) => `
      <li>${option}</li>
  `
    )
    .join("")}
`;
};
// Generate the content for the shootout question
const displayShootoutQuestion = (data) => {
  correctAnswer = data.correct_answer;
  incorrectAnswers = data.incorrect_answers;
  optionsList = incorrectAnswers;
  // Show the section and the question
  gameSection.classList.add("hide");
  questionDiv.classList.add("hide");
  category.classList.add("hide");
  options.classList.add("hide");
  result.classList.add("hide");
  shootoutSection.classList.remove("hide");
  shootoutQuestionElement.innerHTML = `${data.question}`;
};
function showShootoutQuestion(data) {
  // hide the normal question's section
  quizSection.classList.add("hide");
  hideResult();
  // Display on page
  displayShootoutQuestion(data);
}
// Show normal question
function showQuestion(data) {
  // hide result
  hideResult();
  displayQuestion(data);
  // get all answer options
  answerElements = document.querySelectorAll("li");
  // for easy mode CPU has 25% chance to guess (1/4 total answers)
  if (level === "easy") {
    cpuChoiceElement =
      answerElements[Math.floor(Math.random() * answerElements.length)]; // Element chosen by CPU
  } else {
    // separate the incorrect answers from the correct one
    let correctEl,
      incorrectEls = [],
      possibleAnswers;
    answerElements.forEach((element) => {
      element.textContent === correctAnswer
        ? (correctEl = element)
        : incorrectEls.push(element);
    });

    // for medium mode CPU has 33% chance to guess (1/3 total answers)
    if (level === "medium") {
      possibleAnswers = [correctEl, incorrectEls[0], incorrectEls[1]];
      // for hard mode CPU has 50% chance to guess (1/2 total answers)
    } else if (level === "hard") {
      possibleAnswers = [correctEl, incorrectEls[0]];
    }
    cpuChoiceElement =
      possibleAnswers[Math.floor(Math.random() * possibleAnswers.length)];
  }
  selectOption();
}
// Color the answer based on choice (selected answer for player,random answer for cpu)
function selectOption() {
  // allow the user to click on an answer
  options.style.pointerEvents = "all";

  console.log(`CPU choice ${cpuChoiceElement.textContent}`);
  // make the answers clickable
  options.querySelectorAll("li").forEach(function (option) {
    option.addEventListener("click", function (e) {
      // Player's answer is the text of the elementhe clicked on
      let selectedAnswerByPlayer = e.target.textContent;
      // After choosing an option, remove the possibility of choosing another one
      options.style.pointerEvents = "none";
      console.log(`Player's: ${e.target.textContent}`);
      let selectedAnswerByCpu = cpuChoiceElement.textContent;
      if (selectedAnswerByPlayer === selectedAnswerByCpu) {
        // if both choose the same answer
        e.target.classList.add("selected-by-both"); // put a distinct color on the answer
      } else {
        option.classList.add("selected-by-player"); // for player's answer, put player's color
        cpuChoiceElement.classList.add("selected-by-cpu"); // for cpu's answer, put cpu's color
      }
      checkAnswer(selectedAnswerByPlayer, selectedAnswerByCpu); // check the answer
    });
  });
}

function checkAnswer(answerP, answerC) {
  // Convert html elements to normal text
  answerP = HTMLDecode(answerP);
  answerC = HTMLDecode(answerC);
  // Make an array from the node list
  let elementsArray = Array.from(answerElements);
  setTimeout(() => {
    // Correct answer element
    let correctElement = elementsArray[optionsList.indexOf(correctAnswer)];
    result.style.visibility = "visible";
    // Show the result message based on who is the winner
    if (answerP === correctAnswer && answerC !== correctAnswer) {
      result.innerHTML = `<p><i class = "fas fa-check"></i>Correct Answer! Please choose a region</p>`;
      incrementScore("player"); // Increment score
      // Allow to select a region
      locked = false;
      deck.style.pointerEvents = "all";
      winner = "player";
      // Call the function which adds a region
    } else if (answerC === correctAnswer && answerP !== correctAnswer) {
      incrementScore("cpu");
      result.textContent = "Only CPU got it right...He will choose a region.";
      winner = "cpu";
    } else if (answerP === correctAnswer && answerC === correctAnswer) {
      // if both players guessed the answer, they both receive points
      incrementScore("player");
      incrementScore("cpu");

      result.innerHTML = `<p><i class = "fas fa-check">You both got it right! You'll choose a region after Computer's turn! <i class = "fas fa-check"></p>`;
      unlockMap(); // Allow the user to select a territory
      winner = "both";
    }
    // nobody guessed
    else {
      // if none guessed, proceed to the next question
      result.innerHTML = `<p>The correct answer was <span class="correct-word">${correctAnswer}</span>. Let's proceed to the next question....</p> `;
      winner = "none";
      // show a message
      // put another question
      setTimeout(() => {
        loadQuestion();
      }, 3000);
    }
    correctElement.classList.add("correct-answer"); // show which was the correct answer
    if (winner !== "" && winner !== "none") {
      setTimeout(() => {
        // if we have a winner we unlock the map
        locked = false;
        gameSection.classList.remove("hide");
        quizSection.classList.add("hide");
        mapSelector();
      }, 1000);
    }
  }, 3000);
}

// checked if the user selected a territory which is already taken
const isValidAction = (card) => {
  if (
    card.classList.contains("selected-by-player") ||
    card.classList.contains("selected-by-cpu")
  ) {
    return false;
  }
  return true;
};
// update the array which keeps track of which territories are taken
const updateBoard = (index) => {
  triviaMap[index] = "X";
};
// Based on the winner, assign a territory
function mapSelector() {
  if (locked) return;
  cards = document.querySelectorAll(".card");
  switch (winner) {
    case "both":
      cpuTerritory(); // assign a random territory to the CPU
    case "player":
      // Allow the user to choose a territory
      cards.forEach((card, index) => {
        card.addEventListener("click", function (e) {
          if (!isValidAction(card)) return;

          e.target.classList.add("selected-by-player"); // color the map with the player's color
          updateBoard(index); // update the array
          console.log(triviaMap);
          lockMap(); // do not allow the user to choose more than 1 region
          setTimeout(() => {
            loadQuestion(); // next question
            setTimeout(() => {
              gameSection.classList.add("hide");
              quizSection.classList.remove("hide");
            }, 1000);
          }, 1000);
        });
      });
      
      break;
    case "cpu":
      cards = Array.from(document.querySelectorAll(".card"));
      // cpuTerritory(); // assign a random territory to the CPU
      setTimeout(() => {
        loadQuestion();
        setTimeout(() => {
          gameSection.classList.add("hide");
          quizSection.classList.remove("hide");
        }, 2000);
        cpuTerritory(); // assign a random territory to the CPU
      }, 3000);
  }
}
// Cpu gets a random territory, from the ones that are not already taken
function getRandomBox() {
  let randomBox = Math.floor(Math.random() * triviaMap.length);
  // get another random, if the current territory is already taken
  while (triviaMap[randomBox] !== "") {
    randomBox = Math.floor(Math.random() * triviaMap.length);
  }
  return randomBox;
}
// assign a random territory to the CPU
function cpuTerritory() {
  cards = document.querySelectorAll(".card");
  let rand = getRandomBox();
  cards[rand].classList.add("selected-by-cpu");
  updateBoard(rand);
  console.log(triviaMap);
}
// Check how many regions are not taken
const emptyRegions = () => {
  let emptyCounter = 0;
  for (let i = 0; i < triviaMap.length; i++) {
    if (triviaMap[i] === "") emptyCounter++;
  }
  return emptyCounter;
};
// Check who wins at the shootout question
function checkShootout() {
  let shInputValue = +shootoutInput.value; // take user's answer and convert it to number
  let rightAnswer = +correctAnswer;
  let diffPlayer = Math.abs(rightAnswer - shInputValue); // how close the player was to the right answer
  let cpuAnswer = Math.floor(Math.random() * 1000) + 1; // get a random answer for cpu
  let diffCpu = Math.abs(rightAnswer - cpuAnswer); // how close cpu was to the right answer
  console.log(`correct answer: ${rightAnswer}`);
  shootoutReveal.classList.remove("hide");
  // show player and cpu results
  shootoutPlayerResult.textContent = shInputValue;
  shootoutCorrectResult.textContent = correctAnswer;
  shootoutCpuResult.textContent = cpuAnswer;

  setTimeout(() => {
    // who was closer wins
    if (diffPlayer <= diffCpu) {
      shootoutPlayerResult.classList.add("correct-shootout"); // show right answer
      winner = "player";
      unlockMap();
    } else {
      shootoutCpuResult.classList.add("correct-shootout");
      winner = "cpu";
    }
    setTimeout(() => {
      shootoutSection.classList.add("hide");
      gameSection.classList.remove("hide");
      questionDiv.classList.remove("hide");
      category.classList.remove("hide");
      options.classList.remove("hide");
      result.classList.remove("hide");
      quizSection.classList.add("hide");
      locked = false;
      mapSelector();
    }, 1000);
  }, 1000);
}

btnShootout.addEventListener("click", checkShootout);
// Reset the map
function undoMap() {
  triviaMap = ["", "", "", "", "", "", "", "", ""];
  cards.forEach((card) => {
    card.classList.remove("selected-by-player");
    card.classList.remove("selected-by-cpu");
  });
}
// Reset the content and classes from the shootout section
function clearShootout() {
  shootoutReveal.classList.add("hide");
  shootoutPlayerResult.textContent = "";
  shootoutCorrectResult.textContent = "";
  shootoutCpuResult.textContent = "";
  shootoutInput.value = "";
  shootoutPlayerResult.classList.remove("correct-shootout");
  shootoutCpuResult.classList.remove("correct-shootout");
}
// Reset the game
function init() {
  winner = "";
  (locked = true), (isActiveGame = true);
  // resetScores();
  clearShootout();
  undoMap();
}
// Increase the score
const incrementScore = (winner) => {
  if (winner === "player") {
    playerScore += 100;
    scorePlayerElement.textContent = playerScore;
  } else if (winner === "cpu") {
    cpuScore += 100;
    scoreCpuElement.textContent = cpuScore;
  }
};
// Reset the score
const resetScores = () => {
  playerScore = 0;
  cpuScore = 0;
  scorePlayerElement.textContent = 0;
  scoreCpuElement.textContent = 0;
};
// Check who won based on score
const checkWinner = () => {
  // Show a message based on who won the round
  if (playerScore > cpuScore)
    displayMessage("Congratulations!!! You won the game!");
  else if (cpuScore > playerScore)
    displayMessage("This time Computer won. You'll get him next time!");
  else displayMessage("What a battle! It ended with a draw...");
};

const displayMessage = (msg) => {
  messageDesc.textContent = msg;
  messageSection.classList.remove("hide");
  gameSection.classList.add("hide");
  quizSection.classList.add("hide");
  setTimeout(() => {
    messageSection.classList.add("hide");
  }, 1000);
};
// Generate a new question
function loadQuestion() {
  // quizSection.classList.remove("hide");
  const numEmpty = emptyRegions(); // how many territories aren't taken
  // if there are more than 2
  if (numEmpty >= 2) {
    switch (level) {
      // based on what level we are, generate an appropiate question
      case "easy":
        loadEasyQuestion();
        break;
      case "medium":
        loadMediumQuestion();
        break;
      case "hard":
        loadHardQuestion();
        break;
    }
    // when there is only 1 territory left, put the shootout question
  } else if (numEmpty === 1) {
    loadShootoutQuestion();
  } else {
    // if all the territories are taken go to the next level or finish the game
    if (level === "easy") {
      displayMessage(
        "It was only the first level. Let's see what you can do now!"
      );
      init();
      level = "medium";
      loadMediumQuestion();
      return;
    } else if (level === "medium") {
      displayMessage("That was good, but now Computer is set to God-level");
      init();
      level = "hard";
      loadHardQuestion();
      return;
    } else {
      checkWinner();
      // final game message
    }
  }
}

loadQuestion();
