const mainMenuBtn = document.querySelector(".go-back-btn"),
  mainPage = document.querySelector(".main-page"),
  playerInput = document.querySelector(".player-name-input"),
  startBtn = document.querySelector(".start-btn"),
  continueBtn = document.querySelector(".continue-btn"),
  nameInput1=document.getElementById("input1"),
  welcomeMessage=document.getElementById("welcome-message"),
  welcomeSection=document.querySelector(".welcome"),
  continueStory=document.querySelector(".continue-btn-story"),
  bookSection=document.querySelector(".book"),
  story=document.querySelector(".story"),
  mainContaienr=document.querySelector(".main-container"),
  bookPage1=document.getElementById("page1"),
  bookPage2=document.getElementById("page2"),
  bookPage5=document.getElementById("page5"),
  bookPage6=document.getElementById("page6");

mainMenuBtn.addEventListener("click", function () {
  window.location.reload();
});

startBtn.addEventListener("click", function () {
  mainPage.classList.add("hide");
  playerInput.classList.remove("hide");
 mainMenuBtn.classList.remove("hide");
});

//get name from input and put it in span
const playerName = document.querySelector(".fast-flicker");  
playerInput.addEventListener("input", function () {
  playerName.innerHTML = nameInput1.value;
});

continueBtn.addEventListener("click", function () {
    if(nameInput1.value!=""){
         playerInput.classList.add("hide");
         welcomeMessage.innerText=`Welcome, ${nameInput1.value}!`;
         welcomeSection.classList.remove("hide");
         setTimeout(function(){
          mainContaienr.classList.add("hide");
          quizSection.classList.remove("hide");
         }, 3000);
    }
    else if(nameInput1.value==""){
        continueBtn.innerText="Input name!";
        continueBtn.disabled=true;
        setTimeout(function()
        {continueBtn.innerText="Continue";
        continueBtn.disabled=false; }, 1500);
    }
    
})

continueStory.addEventListener("click",function(){
  bookSection.style.animation="fadeOut 2s";
    story.classList.add("hide");
  setTimeout(function(){
    mainContaienr.classList.remove("hide");
  },1000);
    
})

bookPage1.addEventListener("click",function(){
   bookSection.style.animation="slide-left 2s ease-in-out";
   bookSection.style.animationIterationCount=1;
   setTimeout(function(){
    bookSection.style.transform = "translateX(0%)";
   },1500)
})

bookPage2.addEventListener("click",function(){
  bookSection.style.animation="slide-right 2s ";
  bookSection.style.animationIterationCount=1;
  setTimeout(function(){
   bookSection.style.transform = "translateX(-25%)";
  },2000)
})

